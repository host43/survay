package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"gopkg.in/telegram-bot-api.v4"
)

var cFile = "config.json"
var qFile = "steps.json"
var elog *log.Logger
var wlog *log.Logger
var ilog *log.Logger
var dlog *log.Logger
var cfg *Config
var ss *Steps
var us *Users

func init() {
	elog = log.New(os.Stderr, "error: ", log.Lshortfile)
	wlog = log.New(os.Stdout, "warn: ", log.Lshortfile)
	ilog = log.New(os.Stdout, "info: ", log.Lshortfile)
	dlog = log.New(os.Stdout, "debug: ", log.Lshortfile)
	var err error

	flag.StringVar(&cFile, "c", "config.json", "config file")
	flag.StringVar(&qFile, "s", "steps.json", "steps file")
	flag.Parse()

	cfg, err = getConfig(cFile)
	if err != nil {
		elog.Fatal(err)
	}
	ss, err = getSteps(qFile)
	if err != nil {
		elog.Fatal(err)
	}
	us = usersNew()
}

func main() {
	ilog.Println("Starting...")
	bot, err := botNew(cfg.Token, cfg.AdminIds)
	if err != nil {
		elog.Fatal(err)
	}
	ilog.Println(bot)
	bot.Debug = false
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		elog.Fatal(err)
	}

	ticker := time.NewTicker(3 * time.Second)
	go func() {
		timeout := time.Duration(cfg.Timeout) * time.Second
		for _ = range ticker.C {
			uids := us.GetAllUIDs()
			for _, uid := range uids {
				st := us.GetTimestamp(uid)
				now := time.Now()
				diff := now.Sub(st)
				if diff > timeout {
					bot.SendText(uid, "Timeout.")
					send(bot, uid, cfg.LastStep)
					us.Delete(uid)
				}
			}
		}
	}()

	for update := range updates {
		in := update.Message
		q := update.CallbackQuery
		var uid int64
		if in != nil {
			uid = in.Chat.ID
			if in.IsCommand() && !isAdmin(uid) {
				text := in.Text
				switch text {
				case "/start":
					tgname := in.From.FirstName
					ilog.Printf("handle /start from %s", tgname)
					uname := in.From.UserName
					us.NewUser(uid, tgname, uname, 1)
					us.SetSID(uid, 1)
					send(bot, uid, 1)
				}
				continue
			}
		}
		if q != nil {
			uid = q.Message.Chat.ID
		}
		if isAdmin(uid) {
			if in != nil && in.Text != "" {
				uids := us.GetAllUIDs()
				for _, uid := range uids {
					bot.SendText(uid, in.Text)
				}
			}
			continue
		}
		if uid == 0 {
			continue
		}
		if !us.IsExists(uid) {
			bot.SendText(uid, "Please type /start if you want to start.")
			continue
		}
		us.SetTimestamp(uid)
		sid := us.GetSID(uid)
		cs := ss.GetStep(sid)
		identity := us.GetIdentity(uid)
		path := filepath.Join(cfg.StoreDst, time.Now().Format("20060102")+"_"+identity)
		match := false
		if in != nil { // Message handlers
			if in.Photo != nil { //if it is image(photo)
				p := (*in.Photo)[len(*in.Photo)-1]
				fileId := p.FileID
				for _, a := range cs.Answers {
					if a.Type == "image" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					name := fmt.Sprintf("%d.jpg", uid)
					path := filepath.Join(cfg.StoreDst, fmt.Sprintf("%s-%d", us.GetSince(uid).Format("2006-01-02_15-04-05"), uid))
					savePhoto(bot, fileId, path, name)
					//saveFile(bot, fileId, path)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.ResendPhoto(aid, fileId)
					//}
				} else {
					bot.SendText(uid, "We do not wait an image")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Voice != nil {
				p := in.Voice
				fileId := p.FileID
				for _, a := range cs.Answers {
					if a.Type == "voice" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					name := fmt.Sprintf("%d.ogg", uid)
					path := filepath.Join(cfg.StoreDst, fmt.Sprintf("%s-%d", us.GetSince(uid).Format("2006-01-02_15-04-05"), uid))
					saveVoice(bot, fileId, path, name)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.ResendVoice(aid, fileId)
					//}
				} else {
					bot.SendText(uid, "We do not wait an image")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Location != nil { // if it is location
				for _, a := range cs.Answers {
					if a.Type == "location" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					if cs.Attr != "" {
						us.SetAttr(uid, cs.Attr, in.Location)
					}
					bot.RemoveKeyboard(uid)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.SendLocation(aid, in.Location)
					//}
				} else {
					bot.SendText(uid, "We do not wait a location")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Contact != nil {
				for _, a := range cs.Answers {
					if a.Type == "contact" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					if cs.Attr != "" {
						us.SetAttr(uid, cs.Attr, in.Contact)
					}
					bot.RemoveKeyboard(uid)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.SendContact(aid, in.Contact)
					//}
				} else {
					bot.SendText(uid, "We do not wait a contact")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Document != nil {
				p := in.Document
				fileId := p.FileID
				_ = fileId
				for _, a := range cs.Answers {
					if a.Type == "document" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					//saveFile(bot, fileId, path)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.ResendDocument(aid, fileId)
					//}
				} else {
					bot.SendText(uid, "We do not wait a document")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Audio != nil {
				p := in.Audio
				fileId := p.FileID
				_ = fileId
				for _, a := range cs.Answers {
					if a.Type == "audio" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					//saveFile(bot, fileId, path)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.ResendAudio(aid, fileId)
					//}
				} else {
					bot.SendText(uid, "We do not wait an audio")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Video != nil {
				p := in.Video
				fileId := p.FileID
				for _, a := range cs.Answers {
					if a.Type == "video" {
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					saveFile(bot, fileId, path)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//for _, aid := range cfg.AdminIds {
					//	bot.ResendVideo(aid, fileId)
					//}
				} else {
					bot.SendText(uid, "We do not wait a video")
					sid = ss.GetDefaultId(sid)
				}
			}
			if in.Text != "" {
				for _, a := range cs.Answers {
					if a.Type == "text" { //if answer text is present then check text else just type==text
						match = true
						sid = a.NextStep
						break
					}
				}
				if match {
					if cs.Attr != "" {
						us.SetAttr(uid, cs.Attr, in.Text)
					}
					//identity = us.GetIdentity2(uid)
					//sendTextToAdmins(bot, cfg.AdminIds, identity)
					//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
					//sendTextToAdmins(bot, cfg.AdminIds, in.Text)
				} else {
					bot.SendText(uid, "We do not wait a text")
					sid = ss.GetDefaultId(sid)
				}
			}
		}
		if q != nil { //CallbackQuery
			data := q.Data
			for _, a := range cs.Answers {
				if a.Type == "inline" && a.Answer == data {
					match = true
					sid = a.NextStep
					break
				}
			}
			if match {
				//sendTextToAdmins(bot, cfg.AdminIds, identity)
				//sendTextToAdmins(bot, cfg.AdminIds, cs.Text)
				//sendTextToAdmins(bot, cfg.AdminIds, data)
				text := fmt.Sprintf("%s: %s", cs.Text, data)
				bot.EditMessage(uid, q.Message.MessageID, text)
				if cs.Attr != "" {
					us.SetAttr(uid, cs.Attr, data)
				}
			} else {
				bot.SendText(uid, "We do not wait a text")
				sid = ss.GetDefaultId(sid)
			}
		}
		us.SetSID(uid, sid)
		send(bot, uid, sid)
	}
	ticker.Stop()
}

func sendTextToAdmins(bot *Bot, aids []int64, text string) {
	for _, id := range aids {
		bot.SendText(id, text)
	}
}

func saveVoice(bot *Bot, id, path, name string) {
	err := bot.DownloadWithName(id, path, name)
	if err != nil {
		elog.Println(err)
	}
}

func savePhoto(bot *Bot, id, path, name string) {
	err := bot.DownloadWithName(id, path, name)
	if err != nil {
		elog.Println(err)
	}
}

func saveFile(bot *Bot, id string, path string) {
	err := bot.DownloadFile(id, path)
	if err != nil {
		elog.Println(err)
	}
}

func saveUser(uid int64, path string) {
	os.MkdirAll(path, 0777)
	f, err := os.Create(filepath.Join(path, fmt.Sprintf("%d.json", uid)))
	if err != nil {
		elog.Println(err)
		return
	}
	defer f.Close()
	encoder := json.NewEncoder(f)
	encoder.Encode(us.GetUser(uid))
}

func send(bot *Bot, uid int64, sid int) {
	cs := ss.GetStep(sid)
	text := cs.Text
	switch cs.Type {
	case "text":
		bot.SendText(uid, text)
	case "location":
		bot.LocationButton(uid, text)
	case "contact":
		bot.ContactButton(uid, text)
	case "inlineMenu":
		if cs.Buttons == nil {
			elog.Println("buttons object is nil. there is error in json file possible")
			break
		}
		bot.InlineKeyboard(uid, text, cs.Buttons)
	case "document":
		bot.SendDocument(uid, cs.Path)
	}
	if cs.AutoNext != 0 {
		sid := cs.AutoNext
		us.SetSID(uid, sid)
		send(bot, uid, sid)
	}
	if cs.Stop {
		path := filepath.Join(cfg.StoreDst, fmt.Sprintf("%s-%d", us.GetSince(uid).Format("2006-01-02_15-04-05"), uid))
		saveUser(uid, path)
		sendAllDataToAdmin(bot, uid)
		bot.SendText(uid, "Please type /start if you want to start.")
		us.Delete(uid)
	}
}

func sendAllDataToAdmin(bot *Bot, uid int64) { // Multithreading unsafe
	path := filepath.Join(cfg.StoreDst, fmt.Sprintf("%s-%d", us.GetSince(uid).Format("2006-01-02_15-04-05"), uid))
	photoPath := filepath.Join(path, fmt.Sprintf("%d.jpg", uid))
	voicePath := filepath.Join(path, fmt.Sprintf("%d.ogg", uid))
	for _, aid := range cfg.AdminIds {
		text := us.GetIdentity2(uid)
		attrs := us.GetAttrs(uid)
		for a, v := range attrs {
			if value, ok := v.(string); ok {
				text = fmt.Sprintf("%s\n%s: %s", text, a, value)
			}
		}
		//if attr, ok := us.GetAttr(uid, "sex"); ok {
		//	if sex, ok := attr.(string); ok {
		//		text = fmt.Sprintf("%s\n%s %s", text, "Sex:", sex)
		//	}
		//}
		//if attr, ok := us.GetAttr(uid, "living"); ok {
		//	if living, ok := attr.(string); ok {
		//		text = fmt.Sprintf("%s\n%s %s", text, "Living:", living)
		//	}
		//}
		//if attr, ok := us.GetAttr(uid, "citizen"); ok {
		//	if isCitizen, ok := attr.(string); ok {
		//		text = fmt.Sprintf("%s\n%s %s", text, "Is citizen:", isCitizen)
		//	}
		//}
		//wlog.Println(attrs)
		//wlog.Println(text)
		bot.SendText(aid, text)
		//bot.SendText(aid, "Photo:")
		bot.SendPhoto(aid, photoPath)
		//bot.SendText(aid, "Voice:")
		bot.SendVoice(aid, voicePath)
		if attr, ok := us.GetAttr(uid, "location"); ok {
			if location, ok := attr.(*tgbotapi.Location); ok {
				//bot.SendText(aid, "Location:")
				bot.SendLocation(aid, location)
			}
		}
		if attr, ok := us.GetAttr(uid, "contact"); ok {
			if contact, ok := attr.(*tgbotapi.Contact); ok {
				//bot.SendText(aid, "Contact:")
				bot.SendContact(aid, contact)
			}
		}
	}
}

func getConfig(fn string) (*Config, error) {
	fh, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	cfg, err = LoadConfig(fh)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func getSteps(fn string) (*Steps, error) {
	fh, err := os.Open(fn)
	if err != nil {
		return nil, err
	}
	defer fh.Close()
	qs, err := stepsNew(fh)
	if err != nil {
		return nil, err
	}
	return qs, nil
}

func isAdmin(uid int64) bool {
	isadmin := false
	for _, id := range cfg.AdminIds {
		if uid == id {
			isadmin = true
			break
		}
	}
	return isadmin
}
