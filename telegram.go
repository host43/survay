package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"gopkg.in/telegram-bot-api.v4"
)

type Bot struct {
	*tgbotapi.BotAPI
	AdminIds []int64
	client   *http.Client
}

func botNew(token string, aid []int64) (*Bot, error) {
	client := ProxyAwareClient()
	bot, err := tgbotapi.NewBotAPIWithClient(token, client)
	if err != nil {
		return nil, err
	}
	return &Bot{bot, aid, client}, nil
}

func (b *Bot) SendText(id int64, text string) {
	msg := tgbotapi.NewMessage(id, text)
	b.Send(msg)
}

func (b *Bot) LocationButton(id int64, text string) {
	msg := tgbotapi.NewMessage(id, text)
	btn := tgbotapi.NewKeyboardButtonLocation("Send location")
	row := tgbotapi.NewKeyboardButtonRow(btn)
	msg.ReplyMarkup = tgbotapi.NewReplyKeyboard(row)
	b.Send(msg)
}

func (b *Bot) ContactButton(id int64, text string) {
	msg := tgbotapi.NewMessage(id, text)
	btn := tgbotapi.NewKeyboardButtonContact("Send contact")
	row := tgbotapi.NewKeyboardButtonRow(btn)
	msg.ReplyMarkup = tgbotapi.NewReplyKeyboard(row)
	b.Send(msg)
}

func (b *Bot) InlineKeyboard(id int64, text string, btns [][]Button) {
	msg := tgbotapi.NewMessage(id, text)
	keyboard := [][]tgbotapi.InlineKeyboardButton{}
	for _, rowData := range btns {
		row := []tgbotapi.InlineKeyboardButton{}
		for _, btnData := range rowData {
			btn := tgbotapi.NewInlineKeyboardButtonData(btnData.Text, btnData.Data)
			row = append(row, btn)
		}
		keyboard = append(keyboard, row)
	}
	msg.ReplyMarkup = tgbotapi.NewInlineKeyboardMarkup(keyboard...)
	b.Send(msg)
}

func (b *Bot) EditMessage(id int64, mid int, text string) {
	msgedit := tgbotapi.NewEditMessageText(id, mid, text)
	b.Send(msgedit)
}

func (b *Bot) ResendPhoto(id int64, fileId string) {
	msg := tgbotapi.NewPhotoShare(id, fileId)
	b.Send(msg)
}

func (b *Bot) ResendVoice(id int64, fileId string) {
	msg := tgbotapi.NewVoiceShare(id, fileId)
	b.Send(msg)
}

func (b *Bot) ResendAudio(id int64, fileId string) {
	msg := tgbotapi.NewAudioShare(id, fileId)
	b.Send(msg)
}

func (b *Bot) ResendDocument(id int64, fileId string) {
	msg := tgbotapi.NewDocumentShare(id, fileId)
	b.Send(msg)
}

func (b *Bot) ResendVideo(id int64, fileId string) {
	msg := tgbotapi.NewVideoShare(id, fileId)
	b.Send(msg)
}

func (b *Bot) SendLocation(id int64, l *tgbotapi.Location) {
	msg := tgbotapi.NewLocation(id, l.Latitude, l.Longitude)
	b.Send(msg)
}

func (b *Bot) SendContact(id int64, c *tgbotapi.Contact) {
	msg := tgbotapi.NewContact(id, c.PhoneNumber, c.FirstName)
	b.Send(msg)
}

func (b *Bot) RemoveKeyboard(id int64) {
	msg := tgbotapi.NewMessage(id, "Ok")
	msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	msg1, _ := b.Send(msg)
	msg2 := tgbotapi.DeleteMessageConfig{
		id,
		msg1.MessageID,
	}
	b.Send(msg2)
}

func (b *Bot) SendPhoto(id int64, path string) {
	_, err := os.Open(path)
	if err != nil {
		wlog.Println(err)
	}
	msg := tgbotapi.NewPhotoUpload(id, path)
	b.Send(msg)
}

func (b *Bot) SendVoice(id int64, path string) {
	_, err := os.Open(path)
	if err != nil {
		wlog.Println(err)
	}
	msg := tgbotapi.NewVoiceUpload(id, path)
	b.Send(msg)
}

func (b *Bot) SendDocument(id int64, path string) {
	msg := tgbotapi.NewDocumentUpload(id, path)
	b.Send(msg)
}

func (b *Bot) DownloadWithName(id, path, name string) error {
	fileConfig := tgbotapi.FileConfig{id}
	f, err := b.GetFile(fileConfig)
	if err != nil {
		return err
	}
	endpoint := fmt.Sprintf(tgbotapi.FileEndpoint, b.Token, f.FilePath)
	//fmt.Println(f.FilePath)
	resp, err := b.client.Get(endpoint)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("***", path, name)
	os.MkdirAll(path, 0777)
	path = filepath.Join(path, name)
	out, err := os.Create(path)
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}
	return nil
}

func (b *Bot) DownloadFile(id string, path string) error {
	fileConfig := tgbotapi.FileConfig{id}
	f, err := b.GetFile(fileConfig)
	if err != nil {
		return err
	}
	endpoint := fmt.Sprintf(tgbotapi.FileEndpoint, b.Token, f.FilePath)
	//fmt.Println(f.FilePath)
	resp, err := b.client.Get(endpoint)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	srcPath, file := filepath.Split(f.FilePath)
	path = filepath.Join(path, srcPath)
	os.MkdirAll(path, 0777)
	//fmt.Println(path, file)
	path = filepath.Join(path, file)
	out, err := os.Create(path)
	if err != nil {
		return err
	}
	defer out.Close()
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}
	return nil
}
