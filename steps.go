package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Answer struct {
	Type     string `json:"type"`
	Answer   string `json:"answer,omitempty"`
	NextStep int    `json:"next_step"`
}

type Default struct {
	NextStep int `json:"next_step"`
}

type Button struct {
	Text string
	Data string
}

type Question struct {
	Type     string     `json:"type"`
	Text     string     `json:"text"`
	Attr     string     `json:"attrname"`
	AutoNext int        `json:"autonext"`
	Stop     bool       `json:"stop"`
	Path     string     `json:"path"`
	Buttons  [][]Button `json:"buttons"`
	Answers  []Answer   `json:"answers"`
	Default  Default    `json:"default"`
}

type Step struct {
	Id       int      `json:"id"`
	Question Question `json:"step"`
}

type Steps map[int]Question

func stepsNew(f *os.File) (*Steps, error) {
	jsonData, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	var rawSteps []Step
	err = json.Unmarshal(jsonData, &rawSteps)
	if err != nil {
		return nil, err
	}
	steps := Steps{}
	for _, v := range rawSteps {
		steps[v.Id] = v.Question
	}
	return &steps, nil
}

func (s *Steps) GetStep(id int) Question {
	return (*s)[id]
}

func (s *Steps) GetDefaultId(id int) int {
	return (*s)[id].Default.NextStep
}
